#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "../include/readfile.h"

bool readfile( const char *filename, char ** mybufp ){
    FILE *bffile = fopen( filename, "rb" );
    long flen = 0;

    if( ! bffile ){
        printf( "Cannot open file %s\n", filename );
        return false;
    }

    fseek( bffile, 0, SEEK_END );
    flen = ftell( bffile );
    fseek( bffile, 0, SEEK_SET );
    char * mybuf = malloc(flen+1);

    if( mybuf ){
        fread( mybuf, 1, flen, bffile );
        mybuf[flen-1] = '\0';
    }
    else {
        printf( "Cannot allocate memory\n" );
        return false;
    }

    if( fclose( bffile ) != 0 ){
        printf( "Cannot close file\n" );
        return false;
    }
    *mybufp = mybuf;
    return true;
}

void check_args( int argc, const char ** argv, char ** mybuf ){
    const char * filename;

    if( argc > 1 ){
        filename = argv[1];
    }
    else {
        printf("Need filename as argument!\n");
        exit(1);
    }

    if( ! readfile( filename, mybuf ) ){
        exit(1);
    }
}

