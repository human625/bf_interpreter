#include <stdio.h>
#include <stdlib.h>
#include "../include/readfile.h"
#include "../include/bf_inter.h"
#include "../include/rewjump.h"

int main( int argc, const char **argv ){
    char * mybuf = NULL;
    check_args( argc, argv, &mybuf );
    //we want pointer airthmetics here
    char * charp = mybuf;

    Cells * mycells = get_new_mycells();

    while( *charp != '\0' ){
        switch( *charp ){
        case '>':
            rightshift( mycells );
            break;
        case '<':
            leftshift( mycells );
            break;
        case '+':
            increment( mycells );
            break;
        case '-':
            decrement( mycells );
            break;
        case '.':
            outputchar( mycells );
            break;
        case '[':
            if( (*mycells->actchar) == '\0' ){
                jump( &charp, mybuf );
            }
            break;
        case ']':
            if( (*mycells->actchar) != '\0' ){
                rew( &charp, mybuf );
            }
            break;
        }
        charp++;
    }

    free( mybuf );
    free( mycells->myar );

    return 0;
}

