#include <stdlib.h>
#include <stdio.h>
#include "../include/bf_inter_ll.h"

Cells * get_new_mycells(){
    Cells * mycells = malloc(sizeof(Cells));
    mycells->next = NULL;
    mycells->prev = NULL;
    mycells->mychar = '\0';
    mycells->cellnum = 0;
    return mycells;
}

// we want to change tail in place, that's why pointer to pointer
void append( char elem, Cells ** tail ){
    Cells * tailp = *tail;
    tailp->next = (Cells*)malloc(sizeof(Cells));
    if( tailp->next == NULL ){
        printf( "Cannot allocate more memory\n" );
    }
    tailp->next->mychar = elem;
    // fucking segfault because I forgot the line below... arrrghhhh...
    tailp->next->next = NULL;
    tailp->next->prev = tailp;
    tailp->next->cellnum = tailp->cellnum+1;
    (*tail) = tailp->next;
}

void rightshift( Cells **mycells ){
    Cells * mycellsp = *mycells;
    if( mycellsp->next == NULL ){
        append( '\0', mycells );
    }
    else{
        (*mycells) = (*mycells)->next;
    }
}

void leftshift( Cells **mycells ){
    if( (*mycells)->prev != NULL ){
        (*mycells) = (*mycells)->prev;
    }
}

void increment( Cells *mycells ){
    mycells->mychar++;
}

void decrement( Cells *mycells ){
    mycells->mychar--;
}

void outputchar( const Cells *mycells ){
    printf( "%c", mycells->mychar );
    fflush(stdout);
}

void freecells( Cells **head ){
    while( *head != NULL ){   
        Cells * nexthead = (*head)->next;
        free( *head );
        *head = nexthead;
    }
}
