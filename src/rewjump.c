#include "../include/rewjump.h"

void jump( char **charp, const char * mybuf ){
    unsigned int obrackets = 0;
    /* (*charp)++; */
    while( *charp != mybuf ){
        if( **charp == '[' ){
            obrackets++;
        }
        else if( **charp == ']' ){
            obrackets--;
        }

        if( **charp == ']' && obrackets == 0 ){
            break;
        }
        (*charp)++;
    }
}

void rew( char **charp, const char * mybuf ){
    unsigned int cbrackets = 0;
    while( *charp != mybuf ){
        if( **charp == ']' ){
            cbrackets++;
        }
        else if( **charp == '[' ){
            cbrackets--;
        }
        if( **charp == '[' && cbrackets == 0 ){
            break;
        }
        (*charp)--;
    }
}

