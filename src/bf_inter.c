#include "../include/bf_inter.h"
#include <stdlib.h>
#include <stdio.h>

static const int cells_bufsize = 500;

Cells * get_new_mycells(){
    Cells * mycells = malloc(sizeof(Cells));
    mycells->mysize = cells_bufsize;
    mycells->position = 0;
    mycells->myar = calloc( cells_bufsize, 1 );
    mycells->actchar = mycells->myar;
    return mycells;
}

void rightshift( Cells *mycells ){
    if( mycells->position < ((int)mycells->mysize - 1 )){
        mycells->position++;
        mycells->actchar++;
    }
}

void leftshift( Cells *mycells ){
    if( mycells->position > 0 ){
        mycells->position--;
        mycells->actchar--;
    }
}

void increment( Cells *mycells ){
    (*mycells->actchar)++;
}

void decrement( Cells *mycells ){
    (*mycells->actchar)--;
}

void outputchar( const Cells *mycells ){
    printf( "%c", *mycells->actchar );
    fflush(stdout);
}


