#ifndef BF_READFILE_HEADER
#define BF_READFILE_HEADER

#include <stdbool.h>

// https://stackoverflow.com/questions/174531/how-to-read-the-content-of-a-file-to-a-string-in-c
bool readfile( const char *filename, char ** mybuf );
void check_args( int argc, const char ** argv, char ** mybuf );

#endif /* BF_READFILE_HEADER */
