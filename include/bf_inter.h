#ifndef BF_INTER_HEADER
#define BF_INTER_HEADER

#include <stdlib.h>

static const int cells_bufsize;

typedef struct Cells Cells;

struct Cells {
    size_t mysize;
    int position;
    char *actchar;
    char *myar;
};

Cells * get_new_mycells();

void rightshift( Cells *mycells );
void leftshift( Cells *mycells );
void increment( Cells *mycells );
void decrement( Cells *mycells );
void outputchar( const Cells *mycells );

#endif /* BF_INTER_HEADER */
