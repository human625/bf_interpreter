#ifndef BF_INTER_LL_HEADER
#define BF_INTER_LL_HEADER

typedef struct Cells Cells;

// doubly linked list
struct Cells {
    char mychar;
    int cellnum;
    Cells *next;
    Cells *prev;
};

Cells * get_new_mycells();

void append( char elem, Cells ** tail );
void rightshift( Cells **mycells );
void leftshift( Cells **mycells );
void increment( Cells *mycells );
void decrement( Cells *mycells );
void outputchar( const Cells *mycells );
void freecells( Cells **head );

#endif /* BF_INTER_LL_HEADER */
